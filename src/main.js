import { createApp } from 'vue'
import App from './App.vue'
import  VueGoogleMaps from '@fawmi/vue-google-maps'
//createApp(App).mount('#app')
const app = createApp(App);
app.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyAWbF76ktjZtKI0zLBy6cQ8usKFDixTCVY',
    },
}).mount('#app')